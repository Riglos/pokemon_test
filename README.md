# Search Pokemon App .

## Build Setup

```bash
# install dependencies
expo install

# serve with hot reload at localhost:8080
expo start

# build APK for production
expo build:android
```

## Description

It is an application that allows the user searchs Pokemons, autocompleting the word, and allows you to bring us additional information about the Pokemon you are looking for.

## Notes

- `react-native-elements` for user interface, `AXIOS` for the API call and `expo-constants` to handle the size of the statusheader.
