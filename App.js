import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import Constants from "expo-constants";
import PokeSearch from "./src/screens/PokeSearch";

import useGlobalState from "./src/store/useGlobalState";
import Context from "./src/store/context";

export default function App() {
  const store = useGlobalState();
  return (
    <Context.Provider value={store}>
      <SafeAreaView style={styles.container}>
        <PokeSearch />
      </SafeAreaView>
    </Context.Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
    backgroundColor: "#fff"
  }
});
