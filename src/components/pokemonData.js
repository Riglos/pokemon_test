import React, { useState, useContext } from "react";
import { Card, Text, Button, Icon, ListItem } from "react-native-elements";
import Context from "../store/context";
import { View, StyleSheet } from "react-native";

export default function PokemonData() {
  const { state } = useContext(Context);
  const [more, setMore] = useState(false);

  const handleMore = () => {
    setMore(!more);
  };

  const moreData = (
    <View>
      <Text style={styles.title}>Types</Text>
      {state.pokemon.types.map((item, i) => (
        <ListItem key={i} title={item.type.name} bottomDivider />
      ))}
      <Text style={styles.title}>Abilities</Text>
      {state.pokemon.abilities.map((item, i) => (
        <ListItem key={i} title={item.ability.name} bottomDivider />
      ))}
    </View>
  );
  return (
    <Card
      titleStyle={styles.mainTitle}
      title={state.pokemon.name}
      image={{ uri: state.pokemon.sprites.front_default }}
    >
      <View style={styles.contentCard}>
        <Text style={styles.weight}>Weight: {state.pokemon.weight}</Text>
        <Text style={styles.height}>Height: {state.pokemon.height}</Text>
      </View>
      <Button
        onPress={handleMore}
        icon={<Icon name={more ? "remove" : "add"} color="#ffffff" />}
        buttonStyle={styles.buttonStyle}
        title=" INFO"
      />
      {more ? moreData : null}
    </Card>
  );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: "#d6d7da"
  },
  title: {
    fontSize: 19,
    fontWeight: "bold"
  },
  mainTitle: {
    fontSize: 28,
    fontWeight: "bold"
  },
  weight: {
    fontSize: 18,
    color: "red"
  },
  height: {
    fontSize: 18,
    color: "green"
  },
  buttonStyle: {
    borderRadius: 0,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0
  },
  contentCard: {
    justifyContent: "space-around",
    flexDirection: "row"
  }
});
