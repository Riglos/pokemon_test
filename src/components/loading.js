import React from "react";
import { Text, StyleSheet, View, ActivityIndicator } from "react-native";

export default function Loading() {
  return (
    <View style={styles.loading}>
      <Text style={styles.answer}>Loading..</Text>
      <ActivityIndicator />
    </View>
  );
}

const styles = StyleSheet.create({
  answer: {
    fontSize: 40,
    fontWeight: "bold",
    color: "black"
  },
  loading: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  }
});
