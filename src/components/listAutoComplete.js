import React, { useContext } from "react";
// import TouchableScale from "react-native-touchable-scale";
import { ListItem } from "react-native-elements";
import Context from "../store/context";
import { pressOption } from "../utils/api";

export default function ListAutoComplete() {
  const { state, actions } = useContext(Context);
  return state.dataSearches.map((pokemon, i) => (
    <ListItem
      key={i}
      onPress={() => pressOption(pokemon.url, actions, state)}
      friction={90}
      tension={100}
      activeScale={0.95}
      linearGradientProps={{
        colors: ["#ff570f", "#ff9f0f"],
        start: [1, 0],
        end: [0.2, 0]
      }}
      title={pokemon.name}
      titleStyle={{ color: "white", fontWeight: "bold" }}
      chevronColor="white"
      chevron
      containerStyle={{
        marginHorizontal: 16,
        marginVertical: 8,
        borderRadius: 8
      }}
    />
  ));
}
