import axios from "axios";
import { URL } from "../constants/theme";

export const functionmakeRemoteRequest = ({ state, actions }) => {
  //BRING ALL POKEMONS
  return axios
    .get(`${URL}` + `?limit=151`)
    .then(res => {
      actions({
        type: "setState",
        payload: {
          ...state,
          data: res.data
        }
      });
    })
    .catch(err => {
      actions({
        type: "setState",
        payload: {
          ...state,
          error: err.message
        }
      });
    });
};

export const pressOption = (url, actions, state) => {
  //SEARCH PARTICULAR POKEMON
  return axios
    .get(url)
    .then(res => {
      actions({
        type: "setState",
        payload: {
          ...state,
          pokemon: res.data,
          currentSearch: "",
          dataSearches: []
        }
      });
    })
    .catch(err => {
      actions({
        type: "setState",
        payload: {
          ...state,
          error: err.message
        }
      });
    });
};
