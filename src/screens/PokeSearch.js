import React, { useEffect, useContext } from "react";
import Context from "../store/context";
import { SearchBar } from "react-native-elements";
import { Text } from "react-native-elements";
import { functionmakeRemoteRequest } from "../utils/api";
import ListAutoComplete from "../components/listAutoComplete";
import Loading from "../components/loading";
import PokemonData from "../components/pokemonData";
import { View, StyleSheet } from "react-native";

export default function ListScreen() {
  const { state, actions } = useContext(Context);
  useEffect(() => {
    functionmakeRemoteRequest({ state, actions });
  }, []);

  updateSearch = search => {
    actions({
      type: "setState",
      payload: {
        ...state,
        currentSearch: search,
        dataSearches: state.data.results.filter(pokemon =>
          pokemon.name.includes(search.toLowerCase())
        )
      }
    });
  };

  return (
    <View style={styles.container}>
      {state.data.length != 0 ? (
        <View>
          <View style={[styles.headerContainer]}>
            <SearchBar
              containerStyle={styles.searchBarContainer}
              round
              lightTheme
              placeholder="Type Here..."
              onChangeText={updateSearch}
              value={state.currentSearch}
            />
          </View>
          <View style={styles.content}>
            <ListAutoComplete />
            {state.pokemon.length !== 0 ? <PokemonData /> : null}
          </View>
        </View>
      ) : (
        <Loading />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1
  },
  headerContainer: {
    backgroundColor: "#4F80E1"
  },
  content: {
    paddingVertical: 16
  },
  searchBarContainer: { backgroundColor: "white" }
});
