import React from "react";
import ListAutoComplete from "../components/listAutoComplete";
import Context from "../store/context";
import { shallow, mount } from "enzyme";

describe("ListAutoComplete", () => {
  it("rendering <ListAutoComplete />", () => {
    mount(<ListAutoComplete />);
    // expect(wrapper.text()).toEqual("Hello, world!");
  });

  it("initially displays the pokemon name", () => {
    const wrapper = mount(<ListAutoComplete />);
    expect(wrapper.find("title")).toHaveLength(1);
  });

  it("removes an item", () => {
    const wrapper = mount(<ListAutoComplete />);
    wrapper.find("chevronColor");
    expect(wrapper.text()).toEqual("white");
  });
});
